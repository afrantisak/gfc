import sys
from PyQt5 import QtWidgets


class main_window(QtWidgets.QDialog):

    def __init__(self):
        super(main_window, self).__init__()


def main():
    app = QtWidgets.QApplication(sys.argv)
    w = main_window()
    w.resize(600, 600)
    w.setWindowTitle("GFC")
    w.show()
    return app.exec_()


if __name__ == '__main__':
    sys.exit(main())
